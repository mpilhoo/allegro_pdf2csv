#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage

from streamio import StreamIO

import pdb

ignored_lines = {
    u"Nazwa przedmiotu (nr):",
    u"Suma do zapłaty (uwzględnia cenę przedmiotu, koszty wysyłki):",
    u"Sztuk",
    u"Cena",
    u""
}
PLN = u"zł"


class Page:
    def __init__(self, io, codec='utf-8'):
        self.io = io
        self.codec = codec
        self.last_line = ""
        self.item_count = self.find_number_of_items()
        self.headers = []
        self.titles = []
        self.amounts = []
        self.categorize_lines()

    def get_next_line(self):
        while not self.io.all_is_read():
            line = self.io.readline()
            if line not in ignored_lines:
                self.last_line = line
                return line.decode(self.codec)

    def find_number_of_items(self):
        count = 0
        seek_to = self.io.tell()
        while True:
            line = self.get_next_line()
            if self.line_starts_new(line):
                break
            seek_to = self.io.tell()
            count += 1
        self.io.seek(seek_to)
        ignored_lines.add("1")
        return count

    @staticmethod
    def line_starts_new(line):
        return line.startswith(u"Kupujący")

    @staticmethod
    def ends_with_phone(line):
        no_spaces = line.replace(' ', '').replace('-', '')
        if re.search('\+\d{11,12}$', no_spaces) is not None:
            return True
        if re.search('\d{9}$', no_spaces) is not None:
            return True
        if line.endswith(u'brak'):
            return True
        return False

    def categorize_lines(self):
        reading_header = True

        header = []
        title = []

        while not self.io.all_is_read():
            line = self.get_next_line()
            if line:
                if line.endswith(PLN):
                    self.amounts.extend(self.extract_amounts(line))
                    continue

                if self.line_starts_new(line):
                    if title: 
                        self.titles.append(' '.join(title))
                        title = []
                    reading_header = True

                if reading_header:
                    header.append(line)
                    if self.ends_with_phone(' '.join(header)):
                        self.headers.append(' '.join(header))
                        reading_header = False
                        header = []
                else:
                    title.append(line)

        if title:
            self.titles.append(' '.join(title))

        if  len(self.headers) != self.item_count or \
            len(self.titles) != self.item_count or \
            len(self.amounts) / 2 != self.item_count:
            raise Exception("Bad numbers of recognized data lines\ncount: %d  titles: %d  headers: %d  amounts: %d" \
                            % (self.item_count, len(self.titles), len(self.headers), len(self.amounts)))

    @staticmethod
    def amount(string):
        return tuple(int(part.replace(' ', '')) for part in string[:-(len(PLN)+1)].split(","))

    @staticmethod
    def is_room_or_nr(part):
        cleaned = part
        for sth in (u'/', u' ', u'pokój', u'pok.', u'p.', u'lokal', u'lok.', u'l.', u'nr'):
            cleaned = cleaned.replace(sth, u'')
        return re.search('^\d+\w?\d*$', cleaned)

    @staticmethod
    def get_post_code(s):
        r = re.search("^\d{2}[-\s]*\d{3}\s", s)
        if r:
            return r.group()[:-1]
        else:
            return "ERROR"

    @classmethod
    def extract_reciever_data(cls, reciever_string):
        reciever = {}
        reciever_parts = reciever_string.rsplit(', ', 3)
        if len(reciever_parts) == 3:
            reciever['name'] = reciever_parts[0]
            if reciever_parts[2].find(' ') == 3:
                reciever_parts[2] = reciever_parts[2][:3] + reciever_parts[2][4:]
            post_code = cls.get_post_code(reciever_parts[2])
            reciever['address'] = {
                "street": reciever_parts[1],
                "post_code": post_code,
                "city": reciever_parts[2][len(post_code)+1:]
            }
        elif len(reciever_parts) > 3:
            post_code = cls.get_post_code(reciever_parts[-1])
            if cls.is_room_or_nr(reciever_parts[-2]):
                street = ', '.join(reciever_parts[-3:-1])
                reciever['name'] = ', '.join(reciever_parts[:-3])
            else:
                street = reciever_parts[-2]
                reciever['name'] = ', '.join(reciever_parts[:-2])

            reciever['address'] = {
                "street": street,
                "post_code": post_code,
                "city": reciever_parts[-1][len(post_code)+1:]
            }

        return reciever

    @staticmethod
    def extract_contact_details(contact_details_string):
        email, phone = contact_details_string.split(', ')
        return {
            'email': email,
            'phone': phone
        }

    @classmethod
    def extract_data_from_header(cls, header):
        field_parts = header.split('; ')
        record = {}

        if len(field_parts) == 3:
            buyer, reciever, contact_details = field_parts
            nick, name = buyer[10:].split(' - ')
            record['buyer'] = {
                'name': name,
                'nick': nick
            }
        else:
            buyer, business_owner, reciever, contact_details = field_parts
            nick, business_name = buyer[11:].split(' - ', 1)
            business_name, owner_name = (' '.join((business_name, business_owner))).rsplit(' - ', 1)
            record['buyer'] = {
                'name': owner_name,
                'business': business_name,
                'nick': nick
            }

        record['reciever'] = cls.extract_reciever_data(reciever[9:])
        record['contact_details'] = cls.extract_contact_details(contact_details[17:])
        return record

    @staticmethod
    def extract_auction_data(title):
        m = re.search('^(.*) (\(\d{7,8}\))', title)
        return {
            'id': int(m.groups()[1][1:-1]),
            'title': m.groups()[0]
        }

    @classmethod
    def extract_amounts(cls, amounts_string):
        pos = amounts_string.find(PLN)
        amounts = []
        if pos < len(amounts_string) - len(PLN):
            amounts.append(cls.amount(amounts_string[:pos+len(PLN)]))
            amounts.append(cls.amount(amounts_string[pos+len(PLN)+1:]))
        else:
            amounts.append(cls.amount(amounts_string))

        return amounts

    def get_next_item(self):
        if len(self.headers) == 0:
            return None

        header = self.headers.pop(0)
        title = self.titles.pop(0)
        amounts = self.amounts[:2]
        self.amounts = self.amounts[2:]

        record = self.extract_data_from_header(header)
        record['auction'] = self.extract_auction_data(title)

        if amounts[0] <= amounts[1]:
            record['price'] = amounts[0]
            record['to_pay'] = amounts[1]
        else:
            record['price'] = amounts[1]
            record['to_pay'] = amounts[0]
        return record

    @property
    def is_done(self):
        return len(self.headers) == 0


class PdfDecoder:
    def __init__(self, path, codec='utf-8', caching=True, maxpages=0):
        self.path = path
        self.maxpages = maxpages
        self.caching = caching
        self.codec = codec

        self.resource_manager = PDFResourceManager(caching=caching)
        self.io = StreamIO()
        self.converter = TextConverter(self.resource_manager, self.io,
                                       codec=codec, laparams=LAParams())
        self.pagenos = set()
        self.file = file(path, 'rb')
        self.interpreter = PDFPageInterpreter(self.resource_manager, self.converter)

    def iter_items(self):
        page_i = 0
        record_i = 0
        for pdfpage in PDFPage.get_pages(self.file, self.pagenos,
                                         maxpages=self.maxpages,
                                         caching=self.caching,
                                         check_extractable=True):
            page_i += 1
            print("Strona %d" % page_i)
            page_start = self.io.tell()
            self.interpreter.process_page(pdfpage)
            self.io.seek(page_start)
            page = Page(self.io, self.codec)

            while not page.is_done:
                record_i += 1
                print("  Wpis %d" % record_i)
                yield page.get_next_item()

    def close(self):
        self.file.close()
        self.converter.close()


class read_pdf:
    def __enter__(self, path, codec='utf-8', caching=True, maxpages=0):
        self.reader = PdfDecoder(path, codec, caching, maxpages)

    def __exit__(self, type, value, traceback):
        self.reader.close()
