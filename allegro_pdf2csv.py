#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Documentation, License etc.

@package allegroPdf2csv
'''
import json
import sys

from pdfdecoder import PdfDecoder
from csvencoder import CSVEncoder


def main(argv):
    pdf_path = argv[0]
    csv_path = pdf_path.replace('pdf', 'csv')
    slash = '/' if not sys.platform.startswith('win') else '\\'

    print(u"Wybierz nazwę pliku wyjściowego CSV")
    filename = csv_path.split(slash)[-1]
    filename = raw_input('[%s]: ' % filename) or filename
    csv_path = slash.join(pdf_path.split(slash)[:-1] + [filename])

    print(u"Wybierz kodowanie pliku wyjściowego CSV [utf-8, iso8859_2, cp1250]")
    out_codec = raw_input('[cp1250]: ') or 'cp1250'

    try:
        pdf = PdfDecoder(pdf_path)
        csv = CSVEncoder(csv_path, codec=out_codec)

        for item in pdf.iter_items():
            csv.write(item)

        pdf.close()
        csv.close()
    except Exception as e:
        print("Nie udało się...")
        print(e)
        print("Press [ENTER] exit...")
        raw_input()
        raise e
    print("GOTOWE\nPress [ENTER] exit...")
    raw_input()


if __name__ == '__main__': sys.exit(main(sys.argv[1:]))
