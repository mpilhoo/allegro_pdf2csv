#!/usr/bin/env python
# -*- coding: utf-8 -*-
from cStringIO import StringIO


class StreamIO:
    def __init__(self):
        self.string_io = StringIO()
        self.read_seek = 0
        self.write_seek = 0

    def read(self, s=-1):
        self.string_io.seek(self.read_seek)
        tmp = self.string_io.read(s)
        self.read_seek = self.string_io.tell()
        return tmp

    def readline(self):
        self.string_io.seek(self.read_seek)
        tmp = self.string_io.readline()
        self.read_seek = self.string_io.tell()
        return tmp[:-1]

    def readlines(self):
        self.string_io.seek(self.read_seek)
        tmp = self.string_io.readlines()
        self.read_seek = self.string_io.tell()
        return [line[:-1] for line in tmp]

    def write(self, s):
        self.string_io.seek(self.write_seek)
        self.string_io.write(s)
        self.write_seek = self.string_io.tell()

    def writelines(self, sequence_of_strings):
        self.string_io.seek(self.write_seek)
        self.string_io.writelines((string+'\n' for string in sequence_of_strings))
        self.write_seek = self.string_io.tell()

    def all_is_read(self):
        return self.read_seek == self.write_seek

    def tell(self):
        return self.read_seek

    def seek(self, n):
        self.read_seek = n
        self.string_io.seek(n)
